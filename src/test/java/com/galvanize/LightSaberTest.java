package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LightSaberTest {

    LightSaber myLightSaber;

    @BeforeEach
    public void init(){
        myLightSaber = new LightSaber(12345);
    }

    @Test
    void applicationTest() {
        Application.main();
    }

    @Test
    public void constructorShouldStoreJediSerialNumber(){
        //-----SETUP-----
        //setup in init

        //-----ENACT-----

        //-----ASSERT-----
        assertEquals(12345,myLightSaber.getJediSerialNumber());

        //-----TEARDOWN-----
    }
    @Test
    public void setChargeShouldStoreChargeAmountAndGetChargeShouldReturnIt(){
        //-----SETUP-----
        //setup in init

        //-----ENACT-----
        myLightSaber.setCharge(80f);

        //-----ASSERT-----
        assertEquals(80f,myLightSaber.getCharge());

        //-----TEARDOWN-----
    }
    @Test
    public void setColorShouldStoreColorAndGetColorShouldReturnIt(){
        //-----SETUP-----
        //check default value for color
        assertEquals("green",myLightSaber.getColor());

        //-----ENACT-----
        myLightSaber.setColor("red");

        //-----ASSERT-----
        assertEquals("red",myLightSaber.getColor());

        //-----TEARDOWN-----
    }
    @Test
    public void useShouldReduceMinutesAppropriatelyAndGetMinutesShouldReturnMinutesRemaining(){
        //-----SETUP-----
        assertEquals(600f,myLightSaber.getRemainingMinutes());
        LightSaber expected = new LightSaber(23456);
        expected.setCharge((5/6f)*100);

        //-----ENACT-----
        myLightSaber.use(100f);

        //-----ASSERT-----
        assertEquals(expected.getRemainingMinutes(),myLightSaber.getRemainingMinutes());

        //-----TEARDOWN-----
    }
    @Test
    public void rechargeShouldReturnChargeTo100(){
        //-----SETUP-----
        assertEquals(100.0f,myLightSaber.getCharge());

        //-----ENACT-----
        myLightSaber.use(200);
        myLightSaber.recharge();

        //-----ASSERT-----
        assertEquals(100.0f,myLightSaber.getCharge());

        //-----TEARDOWN-----
    }
    @Test
    public void thisTestIsToMeetArbitraryNumberOfTestsRequirements(){
        //-----SETUP-----

        //-----ENACT-----
        myLightSaber.setColor("blue");

        //-----ASSERT-----
        assertEquals("blue",myLightSaber.getColor());

        //-----TEARDOWN-----
    }
    @Test
    public void thisTestIsAlsoForArbitraryNumberOfTestsRequirements(){
        //-----SETUP-----

        //-----ENACT-----
        myLightSaber.use(600);

        //-----ASSERT-----
        assertEquals(0,myLightSaber.getCharge());

        //-----TEARDOWN-----
    }
}